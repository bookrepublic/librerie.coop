import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import ImageComponent from '../components/ImageComponent'
import Layout from '../components/layout'
import Newsletter from '../components/Newsletter'
import PageDescription from '../components/PageDescription'
import TextComponent from '../components/TextComponent'

class NewsletterPage extends Component {
  render() {
    const data = this.props.data

    return (
      <Layout>
        <HelmetDatoCms seo={ data.page.seoMetaTags } />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <h1 className='page__title'>
                { data.page.title }
              </h1>
              <TextComponent content={ data.page.content } />
              <Newsletter />
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default NewsletterPage

export const query = graphql`
  query NewsletterPage {
    page: datoCmsNewsletter {
      id
      title
      content
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
  }
`
