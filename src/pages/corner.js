import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'

class IndexPage extends Component {
  render() {
    const data = this.props.data

    return (
      <Layout>
        <HelmetDatoCms seo={ data.page.seoMetaTags } />
        <PageDescription content={ data.page } />
        <div className='wrap'>
          <div className='grid'>
            {
              data.corners.edges.map(corner =>
                <div className='corner__inner'>
                  <div className='grid__item width-12-12 lap-4-12'>
                    <div className='corner__image'>
                      <Img
                        style={{
                          position: "absolute",
                          left: 0,
                          top: 0,
                          width: "100%",
                          height: "100%",
                          zIndex: 1
                        }}
                        key={ corner.node.photo.id }
                        sizes={ corner.node.photo.fluid }
                      />
                    </div>
                  </div>
                  <div className='grid__item width-12-12 lap-8-12'>
                    <div className='corner__content'>
                      <div className='corner__title'>
                        <h2>{ corner.node.name }</h2>
                      </div>
                      <div
                        className='corner__address'
                        dangerouslySetInnerHTML={{
                          __html: corner.node.address
                        }}
                      />
                      <div
                        className='corner__description'
                        dangerouslySetInnerHTML={{
                          __html: corner.node.description
                        }}
                      />
                    </div>
                  </div>
                </div>
              )
            }
          </div>
        </div>
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query CornerPage {
    page: datoCmsCornerPage {
      id
      title
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
    corners: allDatoCmsCorner(sort: { fields: [position], order: ASC }) {
      edges {
        node {
          id
          name
          address
          photo {
            id
            fluid(maxWidth: 600) {
              ...GatsbyDatoCmsFluid
            }
          }
          description
        }
      }
    }
  }
`
