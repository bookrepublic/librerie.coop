import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import ImageComponent from '../components/ImageComponent'
import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'
import TextComponent from '../components/TextComponent'

class PrivacyPolicy extends Component {
  render() {
    const data = this.props.data

    return (
      <Layout>
        <HelmetDatoCms seo={ data.page.seoMetaTags } />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <h1 className='page__title'>
                { data.page.title }
              </h1>
              <div className='page__block'>
                <TextComponent content={ data.page.content } />
              </div>
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default PrivacyPolicy

export const query = graphql`
  query PrivacyPolicy {
    page: datoCmsPrivacyPolicy {
      id
      title
      content
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
  }
`
