import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'
import Collaborations from '../components/Collaborations'

const IndexPage = ({ data }) => (
  <Layout>
    <HelmetDatoCms seo={ data.page.seoMetaTags } />
    <PageDescription content={ data.page } />
    <Collaborations collaborations={ data.collaborations.edges } />
  </Layout>
)

export default IndexPage

export const query = graphql`
  query CollaborationPage {
    page: datoCmsCollaborationsPage {
      id
      title
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
    collaborations: allDatoCmsCollaboration(sort: { fields: [position], order: ASC }) {
      edges {
        node {
          id
          name
          coverImage {
            id
            fluid(maxWidth: 600) {
              ...GatsbyDatoCmsFluid
            }
          }
          excerpt
          slug
        }
      }
    }
  }
`
