import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import BoxEvent from '../components/BoxEvent'
import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'
import SectionTitle from '../components/SectionTitle'

class EventPage extends Component {
  render() {
    const data = this.props.data
    const dateNow = new Date(Date.now())

    const futureEvents = []
    const pastEvents = []

    data.events.edges.forEach(event => {
        if (new Date(event.node.date) >= dateNow) {
          futureEvents.push(event)
        }
      }
    )

    return(
      <Layout>
        <HelmetDatoCms seo={ data.page.seoMetaTags } />
        <PageDescription
          content={ data.page }
          noMargin={ true }
        />
        {
          futureEvents.length > 0 &&
            <div>
              <div className='wrap'>
                <SectionTitle
                  title={'I prossimi eventi'}
                />
                <div className='grid'>
                  {
                    futureEvents.length > 0 &&
                      futureEvents.map(event =>
                        <div
                          className='grid__item width-6-12 lap-4-12'
                          key={ event.node.id }
                        >
                          <BoxEvent content={ event.node } link='eventi' />
                        </div>
                      )
                  }
                </div>
              </div>
            </div>
        }
        {
          pastEvents.length > 0 &&
            <div>
              <div className='wrap'>
                <SectionTitle
                  title={'Eventi passati'}
                  noMargin={ true }
                />
                <div className='grid'>
                  {
                    pastEvents.length > 0 &&
                      pastEvents.map(event =>
                        <div
                          className='grid__item width-6-12 lap-3-12'
                          key={ event.node.id }
                        >
                          <Box content={ event.node } link='eventi' />
                        </div>
                      )
                  }
                </div>
              </div>
            </div>
        }
      </Layout>
    )
  }
}

export default EventPage

export const query = graphql`
  query EventsPage {
    page: datoCmsEventsPage {
      title
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
    events: allDatoCmsEvent(sort: { fields: [date], order: ASC }) {
      edges {
        node {
          id
          name
          description
          slug
          location
          coverImage {
            id
            fluid(maxWidth: 900) {
              ...GatsbyDatoCmsFluid
            }
          }
          date
          location
        }
      }
    }
  }
`
