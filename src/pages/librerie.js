import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import BoxEvent from '../components/BoxEvent'
import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'

class IndexPage extends Component {
  render() {
    const data = this.props.data
    const bs = () => {
      let bookshopsByRegion = []

      data.regions.edges.map(region => {
          let bsObject = {}
          bsObject['id'] = region.node.name.toLowerCase().replace(/[^A-Z0-9]/ig, "")
          bsObject['region'] = region.node.name
          bsObject['bookshops'] = []
          data.bookshops.edges.map(bookshop => {
            if (bookshop.node.region.id === region.node.id) {
              let bsObjectBs = {}
              bsObjectBs['coverImage'] = bookshop.node.coverPhoto
              bsObjectBs['name'] = bookshop.node.name
              bsObjectBs['address'] = bookshop.node.address
              bsObjectBs['opening'] = bookshop.node.opening
              bsObjectBs['slug'] = bookshop.node.slug
              bsObject['bookshops'].push(bsObjectBs)
            }
          })
        bookshopsByRegion.push(bsObject)
      })

      return bookshopsByRegion
    }

    return (
      <Layout>
        <HelmetDatoCms seo={ data.page.seoMetaTags } />
        <PageDescription content={ data.page } />
        {
          bs().map(bookShop =>
            bookShop.bookshops.length > 0 &&
              <div className='wrap'>
                <div
                  key={ bookShop.id }
                  class='bookshops__region'
                >
                  <h2>{ bookShop.region }</h2>
                </div>
                <div className='grid'>
                  {
                    bookShop.bookshops.map(bookshop =>
                      <div
                        key={ bookshop.id }
                        className='grid__item width-6-12 lap-4-12'
                      >
                        <BoxEvent
                          content={ bookshop }
                          link={ 'librerie' }
                        />
                      </div>
                    )
                  }
                </div>
              </div>

          )
        }
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query BookshopPage {
    regions: allDatoCmsRegion(sort: { fields: [position], order: ASC }) {
      edges {
        node {
          id
          name
        }
      }
    }
    page: datoCmsBookshopsPage {
      id
      title
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
    bookshops: allDatoCmsBookshop(sort: { fields: [position], order: ASC }) {
      edges {
        node {
          id
          name
          slug
          coverPhoto {
            id
            fluid(maxWidth: 600) {
              ...GatsbyDatoCmsFluid
            }
          }
          region {
            id
            name
          }
          address
          opening
        }
      }
    }
  }
`
