import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'
import ReadingTips from '../components/ReadingTips'

const IndexPage = ({ data }) => (
  <Layout>
    <HelmetDatoCms seo={ data.page.seoMetaTags } />
    <PageDescription content={ data.page } />
    <ReadingTips readingTips={ data.readingTips.edges } />
  </Layout>
)

export default IndexPage

export const query = graphql`
  query ReadingTipsPage {
    page: datoCmsReadingTipsPage {
      id
      title
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
    readingTips: allDatoCmsReadingTip(sort: { fields: [meta___publishedAt], order: DESC }) {
      edges {
        node {
          id
          title
          coverImage {
            id
            fluid(maxWidth: 1920) {
              ...GatsbyDatoCmsFluid
            }
          }
          excerpt
          authors {
            ... on DatoCmsAuthor {
              name
              lastName
            }
            ... on DatoCmsBookseller {
              name
              lastName
            }
          }
          slug
          meta {
            publishedAt
          }
        }
      }
    }
  }
`
