import React, { Component } from 'react'
import { Link, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'

import Banner from '../components/Banner'
import Collaborations from '../components/Collaborations'
import Events from '../components/Events'
import Layout from '../components/layout'
import Newsletter from '../components/Newsletter'
import PathStrip from '../components/PathStrip'
import ReadingTips from '../components/ReadingTips'
import SecondBanner from '../components/SecondBanner'
import Slider from '../components/Slider'

class IndexPage extends Component {
  render() {
    const data = this.props.data
    const dateNow = new Date(Date.now())

    const events = []

    data.allEvents.edges.forEach(event => {
        if (new Date(event.node.date) >= dateNow) {
          events.push(event)
        }
      }
    )

    return(
      <Layout>

        <HelmetDatoCms seo={ data.homepage.seoMetaTags } />
        <Slider />
        <Banner />
        <ReadingTips
          readingTips={ data.allReadingTips.edges }
          home={ true }
        />
        <SecondBanner />
        <PathStrip />
        <Events
          events={ events.slice(0,4) }
          home={ true }
        />
        <Newsletter />
        <Collaborations
          collaborations={ data.allCollaborations.edges }
          home={ true }
        />
      </Layout>
    )
  }
}

export default IndexPage

export const query = graphql`
  query ReadingTips {
    homepage: datoCmsHomepage {
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
    allCollaborations: allDatoCmsCollaboration(filter: { active: { eq: true } } ) {
      edges {
        node {
          id
          name
          excerpt
          slug
          coverImage {
            id
            fluid(maxWidth: 600) {
              ...GatsbyDatoCmsFluid
            }
          }
        }
      }
    }
    allReadingTips: allDatoCmsReadingTip(filter: { featured: {eq: false}}, sort: {fields: [meta___publishedAt], order: DESC}, limit: 6) {
      edges {
        node {
          id
          title
          coverImage {
            id
            fluid(maxWidth: 1920) {
              ...GatsbyDatoCmsFluid
            }
          }
          excerpt
          authors {
            ... on DatoCmsAuthor {
              name
              lastName
            }
            ... on DatoCmsBookseller {
              name
              lastName
            }
          }
          slug
          meta {
            publishedAt
          }
        }
      }
    }
    allEvents: allDatoCmsEvent(sort: { fields: [date], order: ASC }) {
      edges {
        node {
          id
          name
          description
          slug
          coverImage {
            id
            fluid(maxWidth: 400) {
              ...GatsbyDatoCmsFluid
            }
          }
          date
          location
          bookshop {
            id
            name
            address
            coverPhoto {
              id
              fluid(maxWidth: 600) {
                ...GatsbyDatoCmsFluid
              }
            }
            region {
              name
            }
          }
          books {
            id
            isbn
            format
            title
            author
            description
            cover {
              id
              fluid(maxWidth: 500) {
                ...GatsbyDatoCmsFluid
              }
            }
            publisher
            currentPrice
            basePrice
          }
          gallery {
            id
            fluid(maxWidth: 1200) {
              ...GatsbyDatoCmsFluid
            }
          }
          video {
            url
            title
            provider
            width
            height
            providerUid
          }
        }
      }
    }
  }
`
