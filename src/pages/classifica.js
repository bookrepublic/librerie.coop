import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import PageDescription from '../components/PageDescription'
import ReadingTips from '../components/ReadingTips'
import Shelf from '../components/Shelf'

const IndexPage = ({ data }) => (
  <Layout>
    <HelmetDatoCms seo={ data.page.seoMetaTags } />
    <PageDescription content={ data.page } />
    <div className='wrap'>
      <div className='grid--center'>
        <div className='grid__item width-12-12 lap-10-12'>
          <Shelf books={ data.page.books } />
        </div>
      </div>
    </div>
  </Layout>
)

export default IndexPage

export const query = graphql`
  query Ranking {
    page: datoCmsRanking {
      id
      title
      description
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      books {
        id
        isbn
        format
        title
        author
        description
        cover {
          id
          fluid(maxWidth: 500) {
            ...GatsbyDatoCmsFluid
          }
        }
        publisher
        currentPrice
        basePrice
      }
    }
  }
`
