import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import MenuItems from '../MenuItems'
import MenuItemsWhite from '../MenuItemsWhite'
import MenuMobile from '../MenuMobile'

class Menu extends Component {
  constructor(props, context) {
    super(props, context);
    this.state = {
      visible: false
    };
    this.toggleMenu = this.toggleMenu.bind(this);
  }

  toggleMenu() {
    this.setState({
        visible: !this.state.visible
    });
  }

  render() {
    return(
      <StaticQuery
        query={graphql`
          query Menu {
            homepage: datoCmsHomepage {
              logo {
                id
                fluid(maxWidth: 500) {
                  ...GatsbyDatoCmsFluid
                }
              }
              books {
                id
                fluid(maxWidth: 400) {
                  ...GatsbyDatoCmsFluid
                }
              }
            }
          }
        `}
        render = {data =>
          <div className='menu'>
            <div className='menu__books'>
              <Img
                id={ data.homepage.books.id }
                fluid={ data.homepage.books.fluid }
              />
            </div>
            <div className='menu__body'>
              <div className='menu__body__inner'>
                <Link
                  to='/'
                  className='menu__logo'
                >
                  <Img
                    id={ data.homepage.logo.id }
                    fluid={ data.homepage.logo.fluid }
                  />
                </Link>
                <div className='menu__items'>
                  <MenuItems />
                </div>
                <div
                  className='menu__hamburger'
                  onClick={ () => this.toggleMenu() }
                >
                </div>
              </div>
              <MenuMobile isVisible={ this.state } />
            </div>
            <div className='menu__body__inner--white'>
              <div className='menu__items'>
                <MenuItemsWhite />
              </div>
            </div>
          </div>
        }
      />
    )
  }
}

export default Menu
