import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import ButtonCoop from '../ButtonCoop'

class BigBox extends Component {
  render() {

    let content = this.props.content
    let type = this.props.type
    let link = this.props.link

    return(
      <div className='bigbox'>
        <div className='bigbox__body'>
          <Img
            style={{
              position: "absolute",
              left: 0,
              top: 0,
              width: "100%",
              height: "100%",
              zIndex: 1,
              adaptiveHeight: true
            }}
            key={ content.coverImage.id }
            sizes={ content.coverImage.fluid }
          />
          <Link
            className='bigbox__body__inner'
            to={`/${link}/${content.slug}`}
          >
            <div className='bigbox__content'>
              <div className='bigbox__content__body'>
                <div className='formatted-content'>
                  <div className='bigbox__content__inner'>
                    {
                      type != null &&
                        <div className='bigbox__subtitle'>
                          { this.props.type }
                        </div>
                    }
                    <h2 className='bigbox__title'>
                      { content.title || content.name }
                    </h2>
                    {
                      content.authors &&
                        content.authors.length > 0 &&
                        <div className='bigbox__authors'>
                          <span>di </span>
                          {
                            content.authors.map(author =>
                              author.name + " " + author.lastName
                            ).join(', ')
                          }
                        </div>
                    }
                    <div
                      className='bigbox__excerpt'
                      dangerouslySetInnerHTML={{
                        __html: content.excerpt
                      }}
                    />
                    <ButtonCoop button={ content.slug } />
                  </div>
                </div>
              </div>
            </div>
          </Link>
        </div>
      </div>
    )
  }
}

BigBox.propTypes = {
  link: PropTypes.node.isRequired,
}

export default BigBox
