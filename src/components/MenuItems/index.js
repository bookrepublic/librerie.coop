import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

class MenuItems extends Component {
  render() {
    return(
      <div>
        <a
          href='https://libri.librerie.coop/cerca'
          className='menu__item'
        >
          Cerca libri
        </a>
        <Link
          to='/librerie'
          className='menu__item'
        >
          Librerie
        </Link>
        <Link
          to='/percorsi'
          className='menu__item'
        >
          Percorsi
        </Link>
        <Link
          to='/eventi'
          className='menu__item'
        >
          Eventi
        </Link>
        <a
          href='https://aiutami.librerie.coop/'
          target='_blank'
          className='menu__item'
        >
          FAQ
        </a>
      </div>
    )
  }
}

export default MenuItems
