import { Link } from 'gatsby'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import ImageComponent from '../ImageComponent'

class Banner extends Component {

  render() {

    let content = this.props.content

    return(
      <StaticQuery
        query={graphql`
          query SecondBanner {
            banner: datoCmsSecondBanner {
              image {
                id
                fluid(maxWidth: 1920) {
                  ...GatsbyDatoCmsFluid
                }
              }
            }
          }
        `}
        render = {data => (
          <div className='wrap'>
            <div className='grid'>
              <div className='grid__item width-12-12'>
                <div className='banner--second'>
                  <ImageComponent image={ data.banner.image } />
                </div>
              </div>
            </div>
          </div>
        )}
      />
    )
  }
}

export default Banner
