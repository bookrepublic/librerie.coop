import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import Carousel from "react-slick"

class Gallery extends Component {
  render() {
    let settings = {
      infinite: true,
      dots: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 5000,
      adaptiveHeight: true
    }

    let content = this.props.content

    return(
      <Carousel
        {...settings}
        className='gallery'
      >
        {
          content.length > 0 &&
            content.map(slide =>
              <div
                className='gallery__slide'
                key={ slide.id }
              >
                <Img
                  sizes={ slide.fluid }
                />
                {
                  slide.title &&
                    <div className='caption'>
                      { slide.title }
                    </div>
                }
              </div>
            )
        }
      </Carousel>
    )
  }
}

export default Gallery
