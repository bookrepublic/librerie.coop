import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

class ImageHero extends Component {
  render() {

    let content = this.props.content

    return(
      <div className='imagehero'>
        <Img
          className='imagehero__image'
          style={{
            position: "absolute",
            left: 0,
            top: 0,
            width: "100%",
            height: "100%",
            zIndex: 1
          }}
          key={ content.coverImage.id || content.image.id }
          sizes={ content.coverImage.fluid || content.image.fluid }
        />
        <div className='imagehero__overlay' />
        <div className='imagehero__body'>
          <div className='imagehero__body__inner'>
            <div className='wrap'>
              <h1 className='imagehero__heading'>
                { content.title || content.name }
              </h1>
              {
                content.authors &&
                content.authors.length > 0 &&
                  <div className='imagehero__librarian'>
                    <span>di </span>
                    {
                      content.authors.map(author =>
                        author.name + " " + author.lastName
                      ).join(', ')
                    }
                  </div>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

ImageHero.propTypes = {
  content: PropTypes.object.isRequired,
}

export default ImageHero
