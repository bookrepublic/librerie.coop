import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import MenuItems from '../MenuItems'
import MenuItemsWhite from '../MenuItemsWhite'

class MenuMobile extends Component {
  render() {

    const isVisible = this.props.isVisible

    return(
      <>
        <div className={`menumobile ${isVisible.visible ? 'is-open' : '' }`}>
          <MenuItems />
          <MenuItemsWhite />
        </div>
      </>
    )
  }
}

export default MenuMobile
