import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import Box from '../Box'
import SectionTitle from '../SectionTitle'

class ReadingTips extends Component {
  render() {
    const readingTips = this.props.readingTips
    const home = this.props.home
    return(
      <div className='wrap'>
        {
          home &&
            <SectionTitle
              link={'/percorsi'}
              title={"Consigli di lettura"}
              noMargin={ true }
            />
        }
        <div className='grid'>
          {
            readingTips.length > 0 &&
              readingTips.map(readingTip =>
                <div
                  className='grid__item width-6-12 lap-4-12'
                  key={ readingTip.node.id }
                >
                  <Box content={ readingTip.node } link='percorsi' />
                </div>
              )
          }
        </div>
      </div>
    )
  }
}

export default ReadingTips
