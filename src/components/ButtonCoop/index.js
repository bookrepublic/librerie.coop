import React, { Component } from 'react'
import { Link } from 'gatsby'

class ButtonCoop extends Component {
  render() {
    const buttonRev = this.props.rev
    const buttonClass = buttonRev ? "--rev" : ""
    return(
      <div className='buttoncoop'>
        <div className={`button${buttonClass}`}>
          Mostra tutti i libri
        </div>
      </div>
    )
  }
}

export default ButtonCoop
