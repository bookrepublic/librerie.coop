import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import BoxEvents from '../BoxEvent'
import BoxLateral from '../BoxLateral'
import SectionTitle from '../SectionTitle'

class Events extends Component {
  render() {
    const events = this.props.events
    const home = this.props.home

    const firstEvents = events[0]
    const allEvents = events.slice(1)

    return(
      <div className='wrap'>
        {
          home &&
            <SectionTitle
              link={'/eventi'}
              title={"Eventi in libreria"}
            />
        }
        {
          firstEvents &&
            <BoxLateral
              content={ firstEvents.node }
              link={ 'eventi' }
            />
        }
        <div className='grid'>
          {
            allEvents.length > 0 &&
              allEvents.map(event =>
                <div
                  className='grid__item width-6-12 lap-4-12'
                  key={ event.node.id }
                >
                  <BoxEvents content={ event.node } link='eventi' />
                </div>
              )
          }
        </div>
      </div>
    )
  }
}

export default Events
