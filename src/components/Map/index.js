import React, { Component } from "react"
import { compose, withProps } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

class Map extends Component {
  render() {

    const location = this.props.location

    const MyMapComponent = compose(
      withProps({
        googleMapURL:
          "https://maps.googleapis.com/maps/api/js?key=AIzaSyD8kpLLIFoJQC7EDZIVqmVfXQBf4MZbS5k&v=3.exp&libraries=geometry,drawing,places",
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `400px` }} />,
        mapElement: <div style={{ height: `100%` }} />
      }),
      withScriptjs,
      withGoogleMap
    )(props => (
      <GoogleMap defaultZoom={14} defaultCenter={{ lat: location.latitude, lng: location.longitude }}>
        {props.isMarkerShown && (
          <Marker position={{ lat: location.latitude, lng: location.longitude }} />
        )}
      </GoogleMap>
    ));

    return(
      <MyMapComponent isMarkerShown />
    )

  }
}


export default Map
