import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'

import cards from "../../../src/images/cards.png"

class Footer extends Component {

  render() {

    return(
      <div className='footer'>
        <div className='footer__line-1'>
          <div className='wrap'></div>
          <ul className='nav--dots'>
            <li className='nav__item'>
              <Link
                to='/lavora-con-noi'
              >
                Lavora con noi
              </Link>
            </li>
            <li className='nav__item'>
              <a
                href='https://aiutami.librerie.coop/'
                title='Faq'
                target='_blank'
              >
                FAQ
              </a>
            </li>
            <li className='nav__item'>
              <Link
                to='/cookie-policy'
              >
                Cookie Policy
              </Link>
            </li>
            <li className='nav__item'>
              <Link
                to='/privacy-policy'
              >
                Privacy Policy
              </Link>
            </li>
            <li className='nav__item'>
              <a
                href='https://www.facebook.com/librerie.coop'
                className='footer__social'
              >
                <div className='icon--facebook' />
              </a>
            </li>
            <li className='nav__item'>
              <a
                href='https://twitter.com/librerieCoop'
                className='footer__social'
              >
                <div className='icon--twitter' />
              </a>
            </li>
            <li className='nav__item'>
              <a
                href='https://www.instagram.com/librerie.coop'
                className='footer__social'
              >
                <div className='icon--instagram' />
              </a>
            </li>
          </ul>
        </div>
        <div className='wrap'>
          <div className='footer__line-2'>
            <div className='footer__item'>
              <span>
                <p>Librerie.coop - © 2016 - Librerie.coop, C.F. e P.I.: 02591561200 - Società soggetta alla Direzione e Coordinamento di Coop Alleanza 3.0</p>
                <p>Sede legale: Via Villanova 29/7 - 40055 Villanova di Castenaso (Bo)</p>
                <p>Sede amministrativa: Via Trattati Comunitari Europei 1957-2007, 13 - 40127 Bologna</p>
                <p>Mail: <a href='mailto:info@librerie.coop'>info@librerie.coop.it</a></p>
              </span>
            </div>
            <div className='footer__item'>
              <div className='formatted-content'>
                <span>
                  <p>Questo sito supporta in <strong>totale sicurezza</strong> tutti i principali metodi di pagamento grazie a Stripe e Paypal</p>
                  <p className='footer__cards'>
                    <img
                      src={ cards }
                    />
                  </p>
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Footer
