import { Link } from 'gatsby'
import PropTypes from 'prop-types'
import React, { Component } from 'react'

class SectionTitle extends Component {
  render() {

    let title = this.props.title
    let link = this.props.link
    let noMargin = this.props.noMargin
    let marginClass = noMargin ? "--no-margin" : ""

    return(
      <div>
        {
          link &&
            <Link
              className={`sectiontitle${marginClass}`}
              to={ link }
            >
              <h2>{ title }</h2>
            </Link>
        }
        {
          ! link &&
            <div className={`sectiontitle${marginClass}`}>
              <h2>{ title }</h2>
            </div>
        }
      </div>
    )
  }
}

SectionTitle.propTypes = {
  title: PropTypes.node.isRequired,
}

export default SectionTitle
