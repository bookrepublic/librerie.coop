import { Link } from 'gatsby'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import ImageComponent from '../ImageComponent'

class Banner extends Component {

  render() {

    let content = this.props.content

    return(
      <StaticQuery
        query={graphql`
          query Banner {
            banner: datoCmsBanner {
              image {
                id
                fluid(maxWidth: 1920) {
                  ...GatsbyDatoCmsFluid
                }
              }
              link
            }
          }
        `}
        render = {data => (
          <div className='wrap'>
            <div className='grid'>
              <div className='grid__item width-12-12'>
                <div className='banner'>
                  <a href={ data.banner.link }>
                    <ImageComponent image={ data.banner.image } />
                  </a>
                </div>
              </div>
            </div>
          </div>
        )}
      />
    )
  }
}

export default Banner
