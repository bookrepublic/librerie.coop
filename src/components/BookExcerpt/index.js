import React, { Component } from 'react'
import Cover from '../Cover'
import {IntlProvider, FormattedMessage, FormattedNumber} from 'react-intl'

import bookFetcher from '../../utils/bookFetcher.js'

class BookExcerpt extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      book: undefined,
    }
  }

  componentDidMount() {
    let isbn = this.props.book.isbn
    let customBook = this.props.book
    let book = bookFetcher(isbn, customBook, this)
  }

  render() {

    let book = this.props.book

    return(
      <IntlProvider locale="en">
        <div>
        {
          ! this.state.loading && this.state.book.title != '' &&
          <div
            className='shelf__item'
            key={ this.state.book.id }
          >
            <a
              href={`https://libri.librerie.coop/libri/${ this.state.book.slug }`} className='button--primary'
              className='book-excerpt'
            >
              <div className='book-excerpt__inner'>
                <div className='book-excerpt__cover'>
                  <Cover
                    book={ this.state.book }
                    bookCoverUrl={ this.state.book.coverUrl }
                  />
                </div>
                <div className='book-excerpt__title'>
                  { this.state.book.title }
                </div>
                <div className='book-excerpt__author'>
                  { ! Array.isArray(this.state.book.authors) &&
                    this.state.book.authors
                  }
                  { Array.isArray(this.state.book.authors) && this.state.book.authors.length > 0 &&
                    <div>
                      {
                        this.state.book.authors.map((author, i) =>
                          this.state.book.authors.length === i + 1 &&
                            <span key={ author.id }>{ author.name }</span>
                        )
                      }
                      {
                        this.state.book.authors.map((author, i) =>
                          this.state.book.authors.length != i + 1 &&
                            <div>
                              <span>{ author.name }</span>
                              <span>, </span>
                            </div>
                        )
                      }
                    </div>
                  }
                </div>
                <div className='book-excerpt__format'>
                  <span>{ this.state.book.format }</span>
                </div>
                <div className='book-excerpt__price'>
                  <span className='book-excerpt__price'>
                    {
                      this.state.book.basePrice &&
                        <span className='book-excerpt__price__base'>
                          <div>
                            <span>{ this.state.book.basePrice }</span>
                          </div>
                        </span>
                    }
                    <span className='book-excerpt__price__current'>
                      <div>
                        <span>{ this.state.book.currentPrice }</span>
                      </div>
                    </span>
                  </span>
                </div>
              </div>
            </a>
          </div>
        }
        </div>
      </IntlProvider>
    )
  }
}

export default BookExcerpt
