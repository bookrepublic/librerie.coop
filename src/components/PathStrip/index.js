import { Link } from 'gatsby'
import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { StaticQuery, graphql } from 'gatsby'

import ButtonCoop from '../ButtonCoop'
import PathStripBook from '../PathStripBook'

class PathStrip extends Component {
  render() {
    return(
      <StaticQuery
        query={graphql`
          query PathStripe {
            ranking: datoCmsRanking {
              id
              title
              description
              image {
                id
                fluid(maxWidth: 600) {
                  ...GatsbyDatoCmsFluid
                }
              }
              books {
                id
                isbn
                format
                title
                author
                description
                cover {
                  id
                  fluid(maxWidth: 500) {
                    ...GatsbyDatoCmsFluid
                  }
                }
                publisher
                currentPrice
                basePrice
              }
            }
          }
        `}
        render={data => (
          <div className='path-strip'>
            <div className='path-strip__inner'>
              <Link
                className='path-strip__infos'
                to='/classifica'
                style={{
                  backgroundImage: `url(${data.ranking.image.fluid.src})`
                }}
              >
                <div className='path-strip__infos__inner'>
                  <div className='path-strip__infos__inner-2'>
                    <div className='path-strip__title'>
                      <h2>{ data.ranking.title }</h2>
                    </div>
                    <div
                      className='path-strip__description'
                      dangerouslySetInnerHTML={{
                        __html: data.ranking.description
                      }}
                    />
                    <div className='path-strip__actions'>
                      <ButtonCoop
                        button={ '/classifica' }
                        rev={ true }
                      />
                    </div>
                  </div>
                </div>
              </Link>
              <div className='path-strip__books'>
                <div className='path-strip__books__inner'>
                  {
                    data.ranking.books.map(book =>
                      <PathStripBook
                        key={ book.id }
                        book={ book }
                      />
                    )
                  }
                </div>
              </div>
            </div>
          </div>
        )}
      />
    )
  }
}

export default PathStrip
