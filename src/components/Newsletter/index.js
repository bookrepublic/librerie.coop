import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'

class Newsletter extends Component {
  render() {
    return(
      <StaticQuery
        query={graphql`
          query Newsletter {
            homepage: datoCmsHomepage {
              newsletter
            }
          }
        `}
        render={data => (
          <div className='newsletter'>
            <div className='hero'>
              <div className='hero__body'>
                <div className='hero__body__inner'>
                  <div className='wrap'>
                    <div className='hero__heading'>
                      Iscriviti alla newsletter
                    </div>
                    <div
                      className='hero__text'
                      dangerouslySetInnerHTML={{
                        __html: data.homepage.newsletter
                      }}
                    />
                  </div>
                  <div className='hero__actions'>
                    <a
                      href='http://eepurl.com/dNE7Zo'
                      className='buttoncoop'
                      target='_blank'
                    >
                      <div className='button'>
                        Iscriviti
                      </div>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
      />
    )
  }
}

export default Newsletter
