import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

class MenuItemsWhite extends Component {
  render() {
    return(
      <div>
        <a
          href='https://libri.librerie.coop/promozioni'
          className='menu__item--black'
        >
          Promozioni
        </a>
        <a
          href='https://libri.librerie.coop/daily-deal'
          className='menu__item--black'
        >
          Daily Deal
        </a>
        <Link
          to='/classifica'
          className='menu__item--black'
        >
          Classifica
        </Link>
        <Link
          to='/collaborazioni'
          className='menu__item--black'
        >
          Collaborazioni
        </Link>
        <Link
          to='/corner'
          className='menu__item--black'
        >
          Corner
        </Link>
        <Link
          to='/raccolta-punti'
          className='menu__item--black'
        >
          Raccolta Punti
        </Link>
        <Link
          to='/servizi'
          className='menu__item--black'
        >
          Servizi
        </Link>
        <Link
          to='/chi-siamo'
          className='menu__item--black'
        >
          Chi Siamo
        </Link>
      </div>
    )
  }
}

export default MenuItemsWhite
