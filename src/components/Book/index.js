import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import {IntlProvider, FormattedMessage, FormattedNumber} from 'react-intl'

import bookFetcher from '../../utils/bookFetcher.js'
import bookFormat from '../../utils/bookFormat.js'

import Cover from '../Cover'

class Book extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      book: undefined,
    }
  }

  componentDidMount() {
    let isbn = this.props.book.isbn
    let customBook = this.props.book
    let book = bookFetcher(isbn, customBook, this)
  }

  render() {

    return(
      <IntlProvider locale="en">
      <div className='book'>
        {
          ! this.state.loading && this.state.book.title != '' &&
            <div className='book__inner'>
              <div className='grid'>
                <div className='grid__item width-4-12 tab-4-12 desk-3-12'>
                  <div className='book__cover'>
                    <Cover book={ this.state.book } bookCoverUrl={ this.state.book.coverUrl } />
                  </div>
                </div>
                <div className='grid__item width-8-12 tab-8-12 desk-9-12'>
                  <div className='book__metas'>
                    <h2 className='book__title'>
                      { this.state.book.title }
                    </h2>
                    { ! Array.isArray(this.state.book.authors) &&
                      <div className='book__authors'>
                        <div className='book__author'>
                          { this.state.book.authors }
                        </div>
                      </div>
                    }
                    { Array.isArray(this.state.book.authors) && this.state.book.authors.length > 0 &&
                        <div className='book__authors'>
                          {
                            this.state.book.authors.map((author, i) =>
                              <div
                                className='book__author'
                                key={ author.id }
                              >
                                { this.state.book.authors.length === i + 1 &&
                                  <a
                                    href={`https://libri.librerie.coop/autori/${author.id}`}
                                  >
                                    <span>{ author.name }</span>
                                  </a>
                                }
                              </div>
                            )
                          }
                          {
                            this.state.book.authors.map((author, i) =>
                              <div
                                className='book__author'
                                key={ author.id }
                              >
                                { this.state.book.authors.length != i + 1 &&
                                  <a
                                    href={`https://libri.librerie.coop/autori/${author.id}`}
                                  >
                                    <span>{ author.name }</span>
                                    <span>, </span>
                                  </a>
                                }
                              </div>
                            )
                          }
                        </div>
                    }
                    <div className='book__cta'>
                      { this.state.book.currentPrice &&
                          <div className="book__cta__price">
                            <span className='book__price__label'>Prezzo</span>
                              <div className='book__price'>
                                { this.state.book.basePrice &&
                                  <span className='book__price__base'>
                                    <div>
                                      { this.state.book.basePrice}
                                    </div>
                                  </span>
                                }
                                <span className='book__price__current'>
                                  <div>
                                    { this.state.book.currentPrice}
                                  </div>
                                </span>
                              </div>
                          </div>
                      }
                      { this.state.book.slug &&
                        <div className='book__cta__button'>
                          <a href={`https://libri.librerie.coop/libri/${ this.state.book.slug }`} className='button--primary'>
                            <span>Vai alla scheda libro</span>
                          </a>
                        </div>
                      }
                      { this.state.book.format
                        && (
                          <li className="book-infos__item book-infos__formats">
                            <h6>FORMATO</h6>
                            <p className="gamma">{ this.state.book.format }</p>
                            { this.state.book.alternativeFormats.length > 0 && (
                              <div>
                                <p className='space--top-2'>
                                  { this.state.book.alternativeFormats.length > 1
                                    && (
                                      <span>Disponibile anche nei formati:</span>
                                    )
                                  }
                                  { this.state.book.alternativeFormats.length < 2
                                    && (
                                      <span>Disponibile anche nel formato:</span>
                                    )
                                  }
                                </p>
                                <div className="book-infos__items">
                                  { this.state.book.alternativeFormats.map(book => {
                                    let alternativePrice = <FormattedNumber value={book.current_price / 100} minimumFractionDigits={2}/>
                                    let format = bookFormat(book.format)
                                    return(
                                      <a
                                        style={{ marginRight: "10px" }}
                                        href={`https://libri.librerie.coop/libri/${ book.slug }`}
                                        key={book.id}
                                        className="book-infos__format"
                                      >
                                        { format } &euro; { alternativePrice }
                                      </a>
                                    )
                                  }
                                  )}
                                </div>
                              </div>
                            )}
                          </li>
                        )
                      }
                    </div>
                  </div>
                  <div className='formatted-content'>
                    <div
                      className='book__description'
                      dangerouslySetInnerHTML={{
                        __html: this.state.book.description
                      }}
                    />
                  </div>
                </div>
              </div>
            </div>
        }
      </div>
      </IntlProvider>
    )
  }
}

export default Book
