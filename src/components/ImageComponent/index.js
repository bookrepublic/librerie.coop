import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

class ImageComponent extends Component {

  render() {

    let image = this.props.image

    return(
      <div className='image'>
        <Img
          id={ image.id }
          fluid={ image.fluid }
        />
      </div>
    )
  }
}

export default ImageComponent
