import React, { Component } from 'react'

import BookExcerpt from '../BookExcerpt'

class Shelf extends Component {

  render() {

    let books = this.props.books

    return(
      <div className='shelf'>
          { books.map(book =>
            <BookExcerpt
              key={ book.id }
              book={ book }
            />
          )}
      </div>
    )
  }
}

export default Shelf
