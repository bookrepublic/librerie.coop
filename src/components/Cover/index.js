import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import coverFetcher from '../../utils/coverFetcher.js'

class Cover extends Component {
  render() {

    let book = this.props.book
    let bookCoverUrl = this.props.bookCoverUrl

    return(
      <div className='cover'>
        { (book.format == 'PDF' || book.format == 'EPUB') &&
          <div className='cover__ribbon'>
            <span>EBOOK</span>
          </div>
        }
        <img
          src={ bookCoverUrl }
        />
      </div>
    )
  }
}

export default Cover
