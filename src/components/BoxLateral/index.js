import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import moment from 'moment'

class BoxLateral extends Component {
  render() {

    let content = this.props.content
    let link = this.props.link
    let date = moment(content.date).format('DD-MM-YYYY')

    return(
      <div className='box-lateral'>
        <Link
          to={`/${link}/${content.slug}`}
        >
          <div className='grid--middle'>
            <div className='grid__item width-6-12 lap-8-12'>
              <div className='box-lateral__image'>
                <Img
                  style={{
                    position: "absolute",
                    left: 0,
                    top: 0,
                    width: "100%",
                    height: "100%",
                    zIndex: 1
                  }}
                  key={ content.coverImage.id }
                  sizes={ content.coverImage.fluid }
                />
              </div>

            </div>
            <div className='grid__item width-6-12 lap-4-12'>

          <div className='box-lateral__content'>
            <h2 className='box-lateral__title'>
              { content.title || content.name }
            </h2>
            {
              content.authors &&
              content.authors.length > 0 &&
                <div className='box-lateral__authors'>
                  <span>di </span>
                  {
                    content.authors.map(author =>
                      author.name + " " + author.lastName
                    ).join(', ')
                  }
                </div>
            }
            {
              date &&
                <div className='box-lateral__date'>
                  { date }
                </div>
            }
            {
              !content.bookshop && content.address || content.location &&
                <div>
                  <div
                    className='box-lateral__location'
                    dangerouslySetInnerHTML={{
                      __html: content.address || content.location
                    }}
                  />
                  {
                    content.opening &&
                      <div
                        className='box-lateral__location space--top-1'
                        dangerouslySetInnerHTML={{
                          __html: content.opening
                        }}
                      />
                  }
                </div>
            }
            {
              content.bookshop && !content.address && !content.location &&
                <div>
                  <div className='box-lateral__bookshop'>
                    { content.bookshop.name }
                  </div>
                  <div
                    className='box-lateral__location'
                    dangerouslySetInnerHTML={{
                      __html: content.bookshop.address
                    }}
                  />
                </div>
            }
            {
              content.excerpt &&
                <div
                  className='box-lateral__excerpt'
                  dangerouslySetInnerHTML={{
                    __html: content.excerpt
                  }}
                />
            }
          </div>

            </div>
          </div>
        </Link>
      </div>
    )
  }
}

BoxLateral.propTypes = {
  content: PropTypes.object.isRequired,
  link: PropTypes.string.isRequired,
}

export default BoxLateral
