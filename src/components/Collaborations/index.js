import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Link } from 'gatsby'
import Img from 'gatsby-image'

import BoxCollaboration from '../BoxCollaboration'
import SectionTitle from '../SectionTitle'

class Collaborations extends Component {
  render() {
    const collaborations = this.props.collaborations
    const home = this.props.home
    return(
      <div className='wrap'>
        {
          home &&
            <SectionTitle
              link={'/collaborazioni'}
              title={"Collaborazioni"}
              noMargin={ true }
            />
        }
        <div className='grid'>
          {
            collaborations.length > 0 &&
              collaborations.map(collaboration =>
                <div
                  className='grid__item width-6-12 lap-6-12'
                  key={ collaboration.node.id }
                >
                  <BoxCollaboration content={ collaboration.node } link='collaborazioni' />
                </div>
              )
          }
        </div>
      </div>
    )
  }
}

export default Collaborations
