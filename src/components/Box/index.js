import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import moment from 'moment'

class Box extends Component {
  render() {

    let content = this.props.content
    let link = this.props.link
    let date = moment(content.meta.publishedAt).format('DD-MM-YYYY')

    return(
      <div className='box'>
        <Link
          to={`/${link}/${content.slug}`}
        >
          <div className='box__image'>
            <Img
              style={{
                position: "absolute",
                left: 0,
                top: 0,
                width: "100%",
                height: "100%",
                zIndex: 1
              }}
              key={ content.coverImage.id }
              sizes={ content.coverImage.fluid }
            />
          </div>
          <div className='box__content'>
            <h2 className='box__title'>
              { content.title || content.name }
            </h2>
            {
              content.authors &&
              content.authors.length > 0 &&
                <div className='box__authors'>
                  <span>di </span>
                  {
                    content.authors.map(author =>
                      author.name + " " + author.lastName
                    ).join(', ')
                  }
                </div>
            }
            {
              date &&
                <div className='box__date'>
                  { date }
                </div>
            }
            {
              !content.bookshop && (content.address || content.location) &&
                <div>
                  <div
                    className='box__location'
                    dangerouslySetInnerHTML={{
                      __html: content.location || content.address
                    }}
                  />
                  {
                    content.opening &&
                      <div
                        className='box__location space--top-1'
                        dangerouslySetInnerHTML={{
                          __html: content.opening
                        }}
                      />
                  }
                </div>
            }
            {
              content.bookshop && (!content.address && !content.location)  &&
                <div>
                  <div className='box__bookshop'>
                    { content.bookshop.name }
                  </div>
                  <div
                    className='box__location'
                    dangerouslySetInnerHTML={{
                      __html: content.bookshop.address
                    }}
                  />
                </div>
            }
            {
              content.excerpt &&
                <div
                  className='box__excerpt'
                  dangerouslySetInnerHTML={{
                    __html: content.excerpt
                  }}
                />
            }
          </div>
        </Link>
      </div>
    )
  }
}

Box.propTypes = {
  content: PropTypes.object.isRequired,
  link: PropTypes.string.isRequired,
}

export default Box
