import CookieConsent, { Cookies } from "react-cookie-consent"
import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'

import Menu from './Menu'
import Footer from './Footer'

import "slick-carousel/slick/slick.css"
import "slick-carousel/slick/slick-theme.css"

require('typeface-open-sans')
require('typeface-gentium-book-basic')

import '../assets/stylesheets/application.sass'

require('normalize.css')

const Layout = ({ children }) => (
  <StaticQuery
    query={graphql`
      query SiteTitleQuery {
        datoCmsSite {
          faviconMetaTags {
            ...GatsbyDatoCmsFaviconMetaTags
          }
        }
        site {
          siteMetadata {
            title
          }
        }
      }
    `}
    render={data => (
      <>
        <CookieConsent
          location="top"
          buttonText="Ho capito"
          containerClasses="cookie"
          buttonClasses="cookie__button"
          cookieName="CookieLibrerieCoop"
          cookieValue="CookieLibrerieCoop"
          buttonStyle={{
            background: 'none',
            color: 'white',
            border: '1',
            borderRadius: '5px',
          }}
        >
            Questo sito utilizza cookie per finalità tecniche. I cookie ti consentono la navigazione del sito e l'uso della Libreria online. Per conoscere l'informativa <a href='https://www.librerie.coop/cookie-policy/'>clicca qui</a>. Continuando a navigare su questo sito, accetti l'uso dei cookie.
        </CookieConsent>
        <Helmet>
          <meta content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no" name="viewport" />
        </Helmet>
        <HelmetDatoCms favicon={data.datoCmsSite.faviconMetaTags} />
        <Menu />
        {children}
        <Footer />
      </>
    )}
  />
)

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
