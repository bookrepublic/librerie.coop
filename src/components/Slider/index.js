import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import Carousel from "react-slick"

import BigBox from '../BigBox'

class Slider extends Component {
  render() {
    let settings = {
      infinite: true,
      dots: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: true,
      autoplay: true,
      autoplaySpeed: 5000,
    }
    return(
      <StaticQuery
        query={graphql`
          query BigBox {
            readingTips: allDatoCmsReadingTip(filter: { featured: {eq: true}}) {
              edges {
                node {
                  id
                  title
                  coverImage {
                    id
                    fluid(maxWidth: 1920) {
                      ...GatsbyDatoCmsFluid
                    }
                  }
                  excerpt
                  authors {
                    ... on DatoCmsAuthor {
                      name
                      lastName
                    }
                    ... on DatoCmsBookseller {
                      name
                      lastName
                    }
                  }
                  slug
                }
              }
            }
          }
        `}
        render = {data =>
          <Carousel
            {...settings}
            className='carousel'
          >
            {
              data.readingTips.edges.length > 0 &&
                data.readingTips.edges.map(readingTip =>
                  <BigBox
                    key={ readingTip.node.id }
                    content={ readingTip.node }
                    type={ "I consigli dei librai"}
                    link={'percorsi'}
                  />
                )
            }
          </Carousel>
        }
      />
    )
  }
}

export default Slider
