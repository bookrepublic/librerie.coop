import React, { Component } from 'react'
import { Link } from 'gatsby'

import bookFetcher from '../../utils/bookFetcher.js'
import Cover from '../Cover'

class PathStripBook extends Component {
  constructor(props) {
    super(props)
    this.state = {
      loading: true,
      book: undefined,
    }
  }

  componentDidMount() {
    let isbn = this.props.book.isbn
    let customBook = this.props.book
    let book = bookFetcher(isbn, customBook, this)
  }

  render() {
    return(
      <>
        {
          ! this.state.loading && this.state.book.title != '' &&
            <a
              href={`https://libri.librerie.coop/libri/${ this.state.book.slug }`}
              className='path-strip__book'
            >
              <div className='path-strip__book__meta'>
                <div className='path-strip__book__title'>
                  { this.state.book.title }
                </div>
                <div className='path-state__book__author'>
                  { ! Array.isArray(this.state.book.authors) &&
                    this.state.book.authors
                  }
                  { Array.isArray(this.state.book.authors) && this.state.book.authors.length > 0 &&
                    <div>
                      {
                        this.state.book.authors.map((author, i) =>
                          this.state.book.authors.length === i + 1 &&
                            <span key={ author.id }>{ author.name }</span>
                        )
                      }
                      {
                        this.state.book.authors.map((author, i) =>
                          this.state.book.authors.length != i + 1 &&
                            <div>
                              <span key={ author.id }>{ author.name }</span>
                              <span>, </span>
                            </div>
                        )
                      }
                    </div>
                  }
                </div>
              </div>
              <div className='path-strip__book__cover'>
                <Cover
                  book={ this.state.book }
                  bookCoverUrl={ this.state.book.coverUrl }
                />
              </div>
            </a>
        }
      </>
    )
  }
}

export default PathStripBook
