import Img from 'gatsby-image'
import React, { Component } from 'react'
import PropTypes from 'prop-types'

class SmallAuthor extends Component {

  render() {

    let author = this.props.author

    return(
      <div className='small-author'>
        <div className='grid--middle'>
          <div className='grid__item width-12-12 tab-4-12 desk-3-12'>
            <div className='small-author__photo'>
              <Img
                style={{
                  position: "absolute",
                  left: 0,
                  top: 0,
                  width: "100%",
                  height: "100%",
                  zIndex: 1,
                  borderRadius: "50%"
                }}
                key={ author.photo.id }
                sizes={ author.photo.fluid }
              />
            </div>
          </div>
          <div className='grid__item width-12-12 tab-8-12 desk-9-12'>
            <div className='small-author__name'>
              <h2>
                {author.name} {author.lastName}
              </h2>
            </div>
            <div
              className='small-author__biography'
              dangerouslySetInnerHTML={{
                __html: author.biography
              }}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default SmallAuthor
