import React, { Component } from 'react'

class PageDescription extends Component {

  render() {

    let content = this.props.content
    let noMargin = this.props.noMargin

    let pageClass = noMargin ? "--no-margin" : ""

    return(
      <div className={`page-description${pageClass}`}>
        <div className='page-description__inner'>
          <div className='page-description__title'>
            <h1>{ content.title }</h1>
          </div>
          <div className='formatted-content'>
            <div
              className='page-description__text'
              dangerouslySetInnerHTML={{
                __html: content.description
              }}
            />
          </div>
        </div>
      </div>
    )
  }
}

export default PageDescription
