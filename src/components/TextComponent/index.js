import React, { Component } from 'react'
import PropTypes from 'prop-types'

class TextComponent extends Component {

  render() {

    let content = this.props.content

    return(
      <div className='formatted-content'>
        <div
          className='text'
          dangerouslySetInnerHTML={{
            __html: content
          }}
        />
      </div>
    )
  }
}

export default TextComponent
