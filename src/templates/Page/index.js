import React, { Component } from 'react'
import { StaticQuery, graphql } from 'gatsby'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import Img from 'gatsby-image'
import { Link } from 'gatsby'

import ImageComponent from '../../components/ImageComponent'
import Layout from '../../components/layout'
import PageDescription from '../../components/PageDescription'
import TextComponent from '../../components/TextComponent'

class StandardPage extends Component {
  render() {
    const data = this.props.data

    return (
      <Layout>
        <HelmetDatoCms seo={ data.page.seoMetaTags } />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <h1 className='page__title'>
                { data.page.title }
              </h1>
              {
                data.page.content.map(block =>
                  <div key={ block.id }>
                    {
                      block.model.apiKey === 'imagetext' &&
                        <div className='page__block'>
                          <div className='grid--middle'>
                            <div className='grid__item width-12-12 tab-4-12'>
                              <ImageComponent image={ block.image } />
                            </div>
                            <div className='grid__item width-12-12 tab-8-12'>

                              <TextComponent content={ block.text } />
                            </div>
                          </div>
                        </div>
                    }
                    {
                      block.model.apiKey === 'text' &&
                        <TextComponent content={ block.text } />
                    }
                    {
                      block.model.apiKey === 'image' &&
                        <ImageComponent image={ block.image } />
                    }
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default StandardPage

export const query = graphql`
  query Page($slug: String!) {
    page: datoCmsPage(slug: { eq: $slug }) {
      id
      title
      content {
        ... on DatoCmsImagetext {
          id
          model { apiKey }
          text
          image {
            id
            fluid(maxWidth: 500) {
              ...GatsbyDatoCmsFluid
            }
          }
        }
        ... on DatoCmsText {
          id
          text
          model { apiKey }
        }
        ... on DatoCmsImage {
          id
          model { apiKey }
          image {
            id
            fluid(maxWidth: 800) {
              ...GatsbyDatoCmsFluid
            }
          }
        }
      }
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
  }
`
