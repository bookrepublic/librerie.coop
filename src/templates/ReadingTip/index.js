import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import { graphql } from 'gatsby'

import Book from '../../components/Book'
import ImageComponent from '../../components/ImageComponent'
import ImageHero from '../../components/ImageHero'
import Layout from '../../components/layout'
import TextComponent from '../../components/TextComponent'

class ReadingTip extends Component {
  render() {

    const readingTip = this.props.data.readingTip

    return(
      <Layout>
        <HelmetDatoCms seo={ readingTip.seoMetaTags } />
        <ImageHero
          content={ readingTip }
        />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <TextComponent content={readingTip.description} />
              {
                readingTip.content.map(block =>
                  <div key={ block.id }>
                    {
                      block.model.apiKey === 'recommended_book' &&
                        <Book book={ block.book } />
                    }
                    {
                      block.model.apiKey === 'text' &&
                        <TextComponent content={ block.text } />
                    }
                    {
                      block.model.apiKey === 'image' &&
                        <ImageComponent image={ block.image } />
                    }
                  </div>
                )
              }
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default ReadingTip

export const query = graphql`
  query IndephtsIndex($slug: String!){
    readingTip: datoCmsReadingTip(slug: { eq: $slug }) {
      title
      coverImage {
        id
        fluid(maxWidth: 1920) {
          ...GatsbyDatoCmsFluid
        }
      }
      excerpt
      description
      authors {
        ... on DatoCmsAuthor {
          id
          name
          lastName
          photo {
            id
            fluid(maxWidth: 300) {
              ...GatsbyDatoCmsFluid
            }
          }
        }
        ... on DatoCmsBookseller {
          id
          name
          lastName
          photo {
            id
            fluid(maxWidth: 300) {
              ...GatsbyDatoCmsFluid
            }
          }
        }
      }
      content {
        ... on DatoCmsRecommendedBook {
          id
          model { apiKey }
          book {
            id
            isbn
            format
            title
            author
            description
            cover {
              id
              fluid(maxWidth: 500) {
                ...GatsbyDatoCmsFluid
              }
            }
            publisher
            currentPrice
            basePrice
          }
        }
        ... on DatoCmsText {
          id
          text
          model { apiKey }
        }
        ... on DatoCmsImage {
          id
          model { apiKey }
          image {
            id
            fluid(maxWidth: 800) {
              ...GatsbyDatoCmsFluid
            }
          }
        }
      }
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
    }
  }
`
