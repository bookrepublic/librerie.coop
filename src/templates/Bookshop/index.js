import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import { graphql } from 'gatsby'
import ResponsiveEmbed from 'react-responsive-embed'

import Gallery from '../../components/Gallery'
import ImageComponent from '../../components/ImageComponent'
import BoxEvent from '../../components/BoxEvent'
import Layout from '../../components/layout'
import Map from '../../components/Map'
import SectionTitle from '../../components/SectionTitle'
import SmallAuthor from '../../components/SmallAuthor'
import TextComponent from '../../components/TextComponent'

class Bookshop extends Component {
  render() {

    const bookshop = this.props.data.bookshop
    const events = this.props.data.events

    const checkEvents = () => {
      let selectedEvents = []

      events.edges.map((event) => {
        if (event.node.bookshop) {
          if (event.node.bookshop.id === bookshop.id ) {
            selectedEvents.push(event.node)
          }
        }
      })

      return selectedEvents
    }

    const futureEvents = []
    const dateNow = new Date(Date.now())

    checkEvents().forEach(event => {
        if (new Date(event.date) >= dateNow) {
          futureEvents.push(event)
        }
      }
    )

    return(
      <Layout>
        <HelmetDatoCms seo={ bookshop.seoMetaTags } />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <h1 className='bookshop__name'>
                { bookshop.name }
              </h1>
              <div className='bookshop__metas'>
                <div className='bookshop__region'>
                  { bookshop.region.name }
                </div>
                <div
                  className='bookshop__address'
                  dangerouslySetInnerHTML={{
                    __html: bookshop.address
                  }}
                />
                <div
                  className='bookshop__opening'
                  dangerouslySetInnerHTML={{
                    __html: bookshop.opening
                  }}
                />
                <div className='bookshop__contacts'>
                  {
                    bookshop.telephone &&
                      <div className='bookshop__contact'>
                        TEL: { bookshop.telephone}
                      </div>
                  }
                  {
                    bookshop.fax &&
                      <div className='bookshop__contact'>
                        FAX: { bookshop.fax }
                      </div>
                  }
                  {
                    bookshop.email &&
                      <div className='bookshop__contact'>
                        E-MAIL: { bookshop.email }
                      </div>
                  }
                </div>
              </div>
              <div className='bookshop__image'>
                <Img
                  key={ bookshop.coverPhoto.id }
                  sizes={ bookshop.coverPhoto.fluid }
                />
              </div>
              {
                bookshop.description &&
                <TextComponent content={ bookshop.description }/>

              }
              {
                bookshop.mappa &&
                  <div className=''>
                    <Map location={ bookshop.mappa } />
                  </div>
              }
              {
                bookshop.photoGallery.length > 0 &&
                  <div>
                    <SectionTitle
                      title={"La gallery"}
                    />
                    <Gallery content={ bookshop.photoGallery } />
                  </div>
              }
              {
                futureEvents.length > 0 &&
                  <div>
                    <SectionTitle
                      title={"I prossimi eventi in libreria"}
                    />

                      <div className='grid'>
                        {
                          futureEvents.length > 0 &&
                            futureEvents.map(event =>
                              <div
                                className='grid__item width-6-12'
                                key={ event.id }
                              >
                                <BoxEvent content={ event } link='eventi' />
                              </div>
                            )
                        }
                      </div>

                  </div>
              }
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default Bookshop

export const query = graphql`
  query BookshopIndex($slug: String!){
    bookshop: datoCmsBookshop(slug: { eq: $slug }) {
      name
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      id
      coverPhoto {
        id
        fluid(maxWidth: 1920) {
          ...GatsbyDatoCmsFluid
        }
        title
      }
      photoGallery {
        id
        fluid(maxWidth: 1920) {
          ...GatsbyDatoCmsFluid
        }
        title
      }
      region {
        id
        name
      }
      address
      telephone
      fax
      email
      opening
      description
      mappa {
        latitude
        longitude
      }
    }
    events: allDatoCmsEvent(sort: { fields: [date], order: ASC }) {
      edges {
        node {
          id
          name
          description
          slug
          coverImage {
            id
            fluid(maxWidth: 400) {
              ...GatsbyDatoCmsFluid
            }
          }
          date
          location
          bookshop {
            id
          }
        }
      }
    }
  }
`
