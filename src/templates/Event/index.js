import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import { graphql } from 'gatsby'
import ResponsiveEmbed from 'react-responsive-embed'

import Book from '../../components/Book'
import Gallery from '../../components/Gallery'
import ImageComponent from '../../components/ImageComponent'
import ImageHero from '../../components/ImageHero'
import Layout from '../../components/layout'
import Map from '../../components/Map'
import SectionTitle from '../../components/SectionTitle'
import SmallAuthor from '../../components/SmallAuthor'
import TextComponent from '../../components/TextComponent'

class Event extends Component {
  render() {

    const event = this.props.data.event

    return(
      <Layout>
        <HelmetDatoCms seo={ event.seoMetaTags } />
        <ImageHero
          content={ event }
        />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <div className='event__meta'>
                {
                  event.location &&
                    <div
                      className='event__location'
                      dangerouslySetInnerHTML={{
                        __html: event.location
                      }}
                    />
                }
                {
                  event.date &&
                    <div className='event__hour'>
                      { event.date }
                    </div>
                }
              </div>
              <TextComponent content={ event.description }/>
              {
                event.bookshop &&
                  <div className='event__bookshop'>
                    <div className='event__bookshop__name'>
                      { event.bookshop.name }
                    </div>
                    <div
                      className='event__location'
                      dangerouslySetInnerHTML={{
                        __html: event.bookshop.address
                      }}
                    />
                    {
                      event.bookshop.mappa &&
                        <div className='event__map'>
                          <Map location={ event.bookshop.mappa } />
                        </div>
                    }
                  </div>
              }
              {
                event.eventAuthors &&
                  <div className='event__authors'>
                    {
                      event.eventAuthors.map(author =>
                        <SmallAuthor
                          key={ author.id }
                          author={ author }
                        />
                      )
                    }
                  </div>
              }
              {
                event.books.length > 0 &&
                    <div className='event__books'>
                      {
                        event.books.map(book =>
                          <Book
                            key={ book.id }
                            book={ book }
                          />
                        )
                      }
                    </div>
              }
              {
                event.gallery.length > 0 &&
                  <div>
                    <SectionTitle
                      title={"La gallery"}
                    />
                    <Gallery content={ event.gallery } />
                  </div>
              }
              {
                event.video &&
                  <div className='event__video'>
                    <SectionTitle
                      title={"Video"}
                    />
                    <ResponsiveEmbed
                      src={`//www.youtube.com/embed/${event.video.providerUid}`}
                      ratio={`${event.video.width}:${event.video.height}`}
                      allowFullScreen
                    />
                  </div>
              }
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default Event

export const query = graphql`
  query EventIndex($slug: String!){
    event: datoCmsEvent(slug: { eq: $slug }) {
      name
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      id
      name
      description
      slug
      coverImage {
        id
        fluid(maxWidth: 1920) {
          ...GatsbyDatoCmsFluid
        }
      }
      date(formatString: "DD-MM-YYYY - kk:mm")
      location
      bookshop {
        id
        name
        address
        coverPhoto {
          id
          fluid(maxWidth: 600) {
            ...GatsbyDatoCmsFluid
          }
        }
        region {
          name
        }
        mappa {
          latitude
          longitude
        }
      }
      books {
        id
        isbn
        format
        title
        author
        description
        cover {
          id
          fluid(maxWidth: 1920) {
            ...GatsbyDatoCmsFluid
          }
        }
        publisher
        currentPrice
        basePrice
      }
      eventAuthors {
        id
        name
        lastName
        biography
        photo {
          id
          fluid(maxWidth: 500) {
            ...GatsbyDatoCmsFluid
          }
        }
      }
      gallery {
        id
        fluid(maxWidth: 1920) {
          ...GatsbyDatoCmsFluid
        }
      }
      video {
        url
        title
        provider
        width
        height
        providerUid
      }
    }
  }
`
