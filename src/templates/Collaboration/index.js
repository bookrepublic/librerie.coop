import React, { Component } from 'react'
import { Link } from 'gatsby'
import Img from 'gatsby-image'
import { HelmetDatoCms } from 'gatsby-source-datocms'
import { graphql } from 'gatsby'
import ResponsiveEmbed from 'react-responsive-embed'

import Book from '../../components/Book'
import Gallery from '../../components/Gallery'
import ImageComponent from '../../components/ImageComponent'
import ImageHero from '../../components/ImageHero'
import Layout from '../../components/layout'
import Map from '../../components/Map'
import SectionTitle from '../../components/SectionTitle'
import SmallAuthor from '../../components/SmallAuthor'
import TextComponent from '../../components/TextComponent'

class Collaboration extends Component {
  render() {

    const collaboration = this.props.data.collaboration

    return(
      <Layout>
        <HelmetDatoCms seo={ collaboration.seoMetaTags } />
        <div className='wrap'>
          <div className='grid--center'>
            <div className='grid__item width-12-12 lap-10-12'>
              <h1 className='collaboration__title'>
                { collaboration.name }
              </h1>
              {
                collaboration.link &&
                  <div className='collaboration__button'>
                    <a
                      className='buttoncoop'
                      href={ collaboration.link }
                      target='_blank'
                    >
                      <div className='button'>
                        Scopri
                      </div>
                    </a>
                  </div>
              }
              <div className='collaboration__image'>
                <a
                  className='buttoncoop'
                  href={ collaboration.link }
                  target='_blank'
                >
                  <Img
                    key={ collaboration.coverImage.id }
                    sizes={ collaboration.coverImage.fluid }
                  />
              </a>
              {
                collaboration.coverImage.title &&
                  <div className='caption'>
                    { collaboration.coverImage.title }
                  </div>

              }
              </div>
              {
                collaboration.description &&
                <TextComponent content={ collaboration.description }/>
              }
              {
                collaboration.photoGallery.length > 0 &&
                  <Gallery content={ collaboration.photoGallery } />
              }
            </div>
          </div>
        </div>
      </Layout>
    )
  }
}

export default Collaboration

export const query = graphql`
  query Collaborations($slug: String!){
    collaboration: datoCmsCollaboration(slug: { eq: $slug }) {
      name
      seoMetaTags {
        ...GatsbyDatoCmsSeoMetaTags
      }
      id
      name
      description
      slug
      link
      coverImage {
        id
        fluid(maxWidth: 1200) {
          ...GatsbyDatoCmsFluid
        }
        title
      }
      photoGallery {
        id
        fluid(maxWidth: 1200) {
          ...GatsbyDatoCmsFluid
        }
        title
      }
    }
  }
`
