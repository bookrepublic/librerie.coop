export default function(isbn, target) {

  async function query(isbn) {
    const response = await fetch('https://catalogue-production.bookrepublic.it/books/' + isbn + '.json')
    return await response.json()
  }

  async function start() {
    const data = await query(isbn)
    let bookData = data.linked.filter(function(elem) { return elem.type === 'book'; })[0];
    const book = []
    if (bookData.cover == '') {
      book["coverUrl"] = 'https://d1csarkz8obe9u.cloudfront.net/posterpreviews/book-cover-flyer-template-6bd8f9188465e443a5e161a7d0b3cf33_screen.jpg'
    } else {
      book["coverUrl"] = bookData.cover
    }
    await target.setState({ loading: false, cover: book })
  }

  start()
    .catch(
      e => {
        const book = []
        book["coverUrl"] = ''
        target.setState({ loading: false, cover: book })
      }
    )
}
