export default function(format) {
  let bookFormat = ''
  if (format == 'paperbacksoftback' || format == 'book' || format == 'otherbookformat') {
    bookFormat = 'Tascabile'
  } else if (format == 'hardback' || format == 'mixedmediaproduct' ) {
    bookFormat = 'Rilegato'
  } else if (format == 'boardbook' ) {
    bookFormat = 'Cartonato'
  } else if (format == 'kindle' || format == 'mobi') {
    bookFormat = 'MOBI'
  } else if (format == 'epub' ) {
    bookFormat = 'EPUB'
  } else if (format == 'pdf' ) {
    bookFormat = 'PDF'
  }

  return bookFormat
}
