import cover from "../../src/images/cover.jpg"
import React, { Component } from 'react'
import {FormattedMessage, FormattedNumber} from 'react-intl'
import bookFormat from './bookFormat'

export default function(isbn, customBook, target) {

  function uniq(myArr, prop) {
    return myArr.filter((obj, pos, arr) => {
        return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
    });
  }

  const renderMoney = (cents) => {
    return <div>
      <span>&euro;&nbsp;</span>
      <FormattedNumber value={cents / 100} minimumFractionDigits={2} />
    </div>;
  }

  async function query(isbn) {
    const response = await fetch('https://catalogue-production.bookrepublic.it/books/' + isbn + '.json')
    return await response.json()
  }

  async function start() {
    const data = await query(isbn)

    let bookData = data.linked.filter(function(elem) { return elem.type === 'book'; })[0]
    let publisherData = data.linked.filter(function(elem) { return elem.type === 'publisher'; })[0]
    let authorsData = data.linked.filter(function(elem) { return elem.type === 'author'; })

    let alternativeFormats = []

    bookData["links"]["alternative_formats"]["ids"].map(alternative =>  {
      let singleBook = data.linked.filter(function(elem) { return elem.type === 'book' && elem.id === alternative; })
      alternativeFormats.push(singleBook[0])
    })

    let authorsName = []
    authorsData.map(author => {
      let authorObj = {}
      authorObj["id"] = author.id
      authorObj["name"] = author.name
      authorsName.push(authorObj)
    })
    let authors = uniq(authorsName)


    const book = []
    book["title"] = customBook.title ? customBook.title : bookData.title
    book["format"] = customBook.format ? customBook.format : bookFormat(bookData.format)
    book["authors"] = customBook.author ? customBook.author : authors
    //book["publisher"] = customBook.publisher ? customBook.publisher : publisherData.name
    book["slug"] = bookData.slug
    book["description"] = customBook.description ? customBook.description : bookData.description
    book["alternativeFormats"] = alternativeFormats
    let currentPrice = bookData.current_price === 0 ? 'Gratis!' : renderMoney(bookData.current_price)
    let basePrice = bookData.base_price === 0 ? 'Gratis!' : renderMoney(bookData.base_price)

    book["currentPrice"] = customBook.current_price ? customBook.current_price : currentPrice
    book["coverUrl"]  = customBook.cover ? customBook.cover.fluid.src : bookData.cover.replace("http:", "")

    if (bookData.current_price != bookData.base_price) {
      book["currentPrice"] = currentPrice
      book["basePrice"] = basePrice
      book["discount"] = Math.round(100 - ((currentPrice * 100) / basePrice));
    } else {
      book["currentPrice"] = currentPrice
    }

    if (book["coverUrl"]  == '') {
      book["coverUrl"] = cover
    }

    target.setState({ loading: false, book: book })
  }

  start()
    .catch(
      e => {
        const book = []
        book["title"] = customBook.title ? customBook.title : ''
        book["format"] = customBook.format ? customBook.format : ''
        book["authors"] = customBook.author ? customBook.author : ''
        //book["publisher"] = customBook.publisher ? customBook.publisher : ''
        book["description"] = customBook.description ? customBook.description : ''
        book["currentPrice"] = customBook.current_price ? customBook.current_price : ''
        book["slug"] = ''
        book["alternativeFormats"] = []
        book["coverUrl"]  = customBook.cover ? customBook.cover.fluid.src : ''

        if (book["coverUrl"]  == '') {
          book["coverUrl"] = cover
        }

        target.setState({ loading: false, book: book })
      }
    )
}
