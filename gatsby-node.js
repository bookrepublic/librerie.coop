const p = require('path')

exports.createPages = ({ actions, graphql }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    resolve(
      graphql(
        `
          {
            readingTips: allDatoCmsReadingTip {
              edges {
                node {
                  id
                  slug
                }
              }
            }
            events: allDatoCmsEvent {
              edges {
                node {
                  id
                  slug
                }
              }
            }
            collaborations: allDatoCmsCollaboration {
              edges {
                node {
                  id
                  slug
                }
              }
            }
            bookshops: allDatoCmsBookshop {
              edges {
                node {
                  id
                  slug
                }
              }
            }
            pages: allDatoCmsPage {
              edges {
                node {
                  id
                  slug
                }
              }
            }
          }
        `
      ).then(result => {
        if (result.errors) {
          reject(result.errors);
        }

        result.data.readingTips.edges.forEach(({ node: readingTips }) => {
          createPage({
            path: `/percorsi/${readingTips.slug}/`,
            component: p.resolve(`./src/templates/ReadingTip/index.js`),
            context: { slug: readingTips.slug }
          });
        });

        result.data.events.edges.forEach(({ node: events }) => {
          createPage({
            path: `/eventi/${events.slug}/`,
            component: p.resolve(`./src/templates/Event/index.js`),
            context: { slug: events.slug }
          });
        });

        result.data.collaborations.edges.forEach(({ node: collaborations }) => {
          createPage({
            path: `/collaborazioni/${collaborations.slug}/`,
            component: p.resolve(`./src/templates/Collaboration/index.js`),
            context: { slug: collaborations.slug }
          });
        });

        result.data.bookshops.edges.forEach(({ node: bookshops }) => {
          createPage({
            path: `/librerie/${bookshops.slug}/`,
            component: p.resolve(`./src/templates/Bookshop/index.js`),
            context: { slug: bookshops.slug }
          });
        });

        result.data.pages.edges.forEach(({ node: pages }) => {
          createPage({
            path: `/${pages.slug}/`,
            component: p.resolve(`./src/templates/Page/index.js`),
            context: { slug: pages.slug }
          });
        });

      })
    );
  });
};
